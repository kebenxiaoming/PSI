Ext.define("PSI.Const", {
	statics : {
		REQUIRED : '<span style="color:red;font-weight:bold" data-qtip="必录项">*</span>',
		LOADING : "数据加载中...",
		SAVING : "数据保存中...",
		BASE_URL : "",
		MOT: "0", // 模块打开方式
		VERSION : "PSI 2018 - build201801011015"
	}
});